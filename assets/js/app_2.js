/*****************  Config + Routes ******************************/

angular.module('CrudApp', ['geolocation','ui.bootstrap','ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.
      when('/all', {templateUrl: 'assets/tpl/list.html', controller: ListCtrl}).
      when('/map', {templateUrl: 'assets/tpl/map.html', controller: MapCtrl}).
      when('/add-point', {templateUrl: 'assets/tpl/form.html', controller: ListCtrl}).
      otherwise({redirectTo: '/'});
}])

.controller('GeoCtrl', function ($scope,geolocation) {
  geolocation.getLocation().then(function(data){
    $scope.coords = {lat:data.coords.latitude, long:data.coords.longitude};
  });
});


/********************** Fonctions ******************************/

var AppControllers = angular.module('AppControllers', []);



function ListCtrl($scope, $http) {
  $http.get('api/index.php/all').success(function(data) {
    $scope.points = data;
  });
  
  
  $http.get('api/index.php/add-point').success(function(data) {
      
  });
  
}


// Contrôleur de la map
AppControllers.controller('MapCtrl', ['$scope','$http',
    function($scope,$http){
        $scope.message = "Bienvenue sur la liste";
         $http.get('api/index.php/map').success(function(data) {
        $scope.types = data; 
        
        //$http.get('api/index.php/').success(function(data) {
            $scope.types = data;

            $scope.getItemsType = function(type) {


                //console.log(type);
               console.log($scope);

                var request = $http({
                    method: "get",
                    url: "api/index.php/points/"+type
                });   


                return request.then(function(response) {

                    $scope.items_type = response.data;
                }, function() {
                    console.log('Echec AJAX');
                });

            };

       // });        
        
    })
    } 
]);

/*
function MapCtrl($scope, $http) {
    $http.get('api/index.php/').success(function(data) {
        $scope.types = data;
        
        $scope.getItemsType = function(type) {
            
            
            //console.log(type);
           console.log($scope);
            
            var request = $http({
                method: "get",
                url: "api/index.php/points/"+type
            });   
            
            
            return request.then(function(response) {
                
                $scope.items_type = response.data;
            }, function() {
                console.log('Echec AJAX');
            });

        };
        
    });


    
}
*/
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


