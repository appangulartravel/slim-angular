
'use strict';
var myApp = angular.module('CrudApp.map', ['ngMap'])
.controller('MapCtrl', 
    
function(NgMap) {
    var vm = this;
    NgMap.getMap().then(	    
	function(map) {
	vm.map = map;
	console.log(map.getCenter());
	console.log('markers', map.markers);
	console.log('shapes', map.shapes);
	}	  
    );
    vm.onClick= function(event) {
	vm.geoType =  event.feature.getGeometry().getType();
	vm.geoArray = event.feature.getGeometry().getArray();
	console.dir('geoArray', event.feature.getGeometry().getArray());
    }; 
  
});
