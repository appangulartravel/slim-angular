// js/app.js
'use strict';

/**
 * Déclaration de l'application + des dépendances
 */
var App = angular.module('CrudApp', [
    // Dépendances du "module"
    'ngRoute',
    'AppControllers',
    'ui.bootstrap',
]);



/**
 * Configuration du module principal : routeApp
 */
App.config(['$routeProvider',
    function($routeProvider) { 
        
        // Système de routage
        $routeProvider
//        .when('/', {
//            templateUrl: 'assets/tpl/home.html',
//            controller: 'homeCtrl'
//        })
        .when('/home', {
            templateUrl: 'assets/tpl/map.html',
            controller: 'MapCtrl'
        })
        .when('/around/:cat/:lat/:long/:dist', {
            templateUrl: 'assets/tpl/list.html',
            controller: 'aroundCtrl'
        })
        .when('/points/:cat', {
            templateUrl: 'assets/tpl/list.html',
            controller: 'typesCtrl'
        })
        .when('/all', {
            templateUrl: 'assets/tpl/list.html',
            controller: 'listCtrl'
        })
	   .when('/add-point', {
            templateUrl: 'assets/tpl/form.html',
            controller: 'formCtrl'
        })
	   .when('/cron', {
            templateUrl: 'assets/tpl/list.html',            
            controller: 'BaseCtrl'
        })
        .otherwise({
            redirectTo: '/home'
        });
    }
]);


/**
 * Définition des contrôleurs
 */
var AppControllers = angular.module('AppControllers', ['ngMap']);

// Contrôleur de la page d'accueil
AppControllers.controller('homeCtrl', ['$scope',
    function($scope){
        $scope.message = "Bienvenue sur la page d'accueil"; 

               
    }
]);

/**
 * Controller affichant les points autour d'une position données
 */
AppControllers.controller('aroundCtrl', ['$scope', '$routeParams','$http',
    function($scope, $routeParams, $http){
        // Pour afficher les informations
        $scope.cat = $routeParams.cat;
        $scope.lat = $routeParams.lat;       
        $scope.long = $routeParams.long;   
        $scope.dist = $routeParams.dist;  
        console.log('api/index.php/around/'+$scope.cat+'/'+$scope.lat+'/'+$scope.long+'/'+$scope.dist);
        $http.get('api/index.php/around/'+$scope.cat+'/'+$scope.lat+'/'+$scope.long+'/'+$scope.dist).success(function(data){
             console.log(data);
             $scope.points = data;
             console.log($scope);
        });
    }
]);


AppControllers.controller('typesCtrl', ['$scope', '$routeParams','$http',
    function($scope, $routeParams, $http){
        // Pour afficher les informations
        $scope.cat = $routeParams.cat;
        console.log('api/index.php/points/'+$scope.cat);       
        $http.get('api/index.php/points/'+$scope.cat).success(function(data){
             console.log(data);
             $scope.points = data;
             console.log($scope);
        });
    }
]);


/**
 */
AppControllers.controller('BaseCtrl', ['$scope', '$http',
    function($scope){
        $scope.message = "Bienvenue sur la page d'accueil";
        $http.get('api/index.php/cron').success(function(data) {
            $scope.message = 'Mise à jour réussie';
        });        
    }
]);

// Contrôleur de la liste
AppControllers.controller('listCtrl', ['$scope','$http',
    function($scope,$http){
        $scope.message = "Bienvenue sur la liste";

  
        
        $http.get('api/index.php/all').success(function(data) {
            $scope.points = data;
        });
    } 
]);


// Contrôleur de la map
AppControllers.controller('MapCtrl', ['$scope','$http' ,
    function($scope,$http){
        $scope.message = "Bienvenue sur la liste";
        $scope.asked_type = 'HOTELLERIE';
        
//        geolocation.getLocation().then(function(data){
//          $scope.coords = {lat:data.coords.latitude, long:data.coords.longitude};
//                      console.log($scope.coords);
//
//        });

        var options = {
                enableHighAccuracy: true
            };
        navigator.geolocation.getCurrentPosition(function(pos) {
                $scope.position = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
                $scope.coords = JSON.stringify($scope.position);
                console.log(JSON.stringify($scope.position));                  
            }, 
            function(error) {                    
                $scope.message='Unable to get location: ' + error.message;
            }, options);
        
        
        $http.get('api/index.php/map').success(function(data) {
            
        $scope.types = data;
        

        //Récupère les Points d'un type donné
        $scope.getItemsType = function(type) {
            
            $scope.type = type;

        
            var request = $http({
                method: "get",
                url: "api/index.php/points/"+type
                });   
            
            
            return request.then(function(response) {
                $scope.items_type = response.data;

                $scope.asked_type = type;


                $('#map').prop('disabled', true);
                //$('#map').removeAttr('disabled', false);
                
                //Ne recharge pas la map...

                
            }, function() {
                console.log('Echec AJAX');
            });

        };        
        
    });
    
    

    } 
]);


/**
 * Contrôleur de la page formulaire
 * TODO : Permet d'ajouter un point
 */ 
AppControllers.controller('formCtrl', ['$scope','$routeParams',
    function($scope, $routeParams){
        $scope.message = "Laissez-nous un message sur la page de contact !";
       // $scope.msg = "Bonne chance pour cette nouvelle appli !";
       // $scope.msg = $routeParams.msg;
        // Si aucun paramètre n'est passé, on met notre phrase initiale
        $scope.msg = $routeParams.msg || "Bonne chance pour cette nouvelle appli !";
    }
]);


/**
 * Controller de la ngMap : ngCtrl
 * Affiche la map et ses marqueurs
 */
AppControllers.controller('ngctrl',
function(NgMap) {
    var vm = this;
    NgMap.getMap().then(	    
	function(map) {
	vm.map = map;
	console.log(map.getCenter());
	console.log('markers', map.markers);
	console.log('shapes', map.shapes);            
	}	  
    );
    vm.onClick= function(event) {
        console.log('coucou');
	vm.geoType =  event.feature.getGeometry().getType();
	vm.geoArray = event.feature.getGeometry().getArray();
	console.dir('geoArray', event.feature.getGeometry().getArray());
    }; 

} 
);

/*
//Directive Map
App.directive("map",function(geolocation){
    
//    geolocation.getLocation().then(function(data, $scope){
//      $scope.coords = {lat:data.coords.latitude, long:data.coords.longitude};
//    });  
    
        
    return{
        restrict: 'E',
        template: '<div></di>',
        replace: true,
        scope: {},
        link: function(scope, element, attrs){
            
            
            geolocation.getLocation().then(function(data){
              scope.coords = {lat:data.coords.latitude, long:data.coords.longitude};
                          console.log(scope.coords);

            });            
            
            console.log(scope.coords);
            var myLatLng = new google.maps.LatLng(0,0);
            var mapOptions = {
                center: myLatLng,
                zoom: 16,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
           // var map = new google.maps.Map(document.getElementByIq("map-canvas"),
            var map = new google.maps.Map(document.getElementById(attrs.id),
                mapOptions);   
             var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'My town'
              });
              
              
              //var loc = geolocation.getCurrentPosition();
                geolocation.getLocation().then(function(data){
                  var coords = {lat:data.coords.latitude, long:data.coords.longitude};
                  //return coords;
                });                   
              //console.log(loc.getValue);
              
              
             //marker.setMap(map);

            }
        };    

    

   
});
*/


 