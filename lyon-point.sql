-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1build0.15.04.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 10 Février 2016 à 09:37
-- Version du serveur :  5.6.28-0ubuntu0.15.04.1
-- Version de PHP :  5.6.4-4ubuntu6.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `lyon-point`
--

-- --------------------------------------------------------

--
-- Structure de la table `points`
--

CREATE TABLE IF NOT EXISTS `points` (
  `ID` int(11) NOT NULL,
  `id_sytra1` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `type` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `type_detail` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `nom` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `adresse` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `codepostal` varchar(5) COLLATE utf8_bin DEFAULT NULL,
  `commune` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `telephone` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `fax` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `telephonefax` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `siteweb` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `facebook` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `classement` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `ouverture` varchar(3000) COLLATE utf8_bin DEFAULT NULL,
  `tarifsenclair` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `tarifsmin` int(11) DEFAULT NULL,
  `tarifsmax` int(11) DEFAULT NULL,
  `producteur` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `gid` int(32) NOT NULL,
  `date_creation` timestamp NULL DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT NULL,
  `last_update_fme` timestamp NULL DEFAULT NULL,
  `lat` float DEFAULT NULL,
  `long` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `points`
--

INSERT INTO `points` (`ID`, `id_sytra1`, `type`, `type_detail`, `nom`, `adresse`, `codepostal`, `commune`, `telephone`, `fax`, `telephonefax`, `email`, `siteweb`, `facebook`, `classement`, `ouverture`, `tarifsenclair`, `tarifsmin`, `tarifsmax`, `producteur`, `gid`, `date_creation`, `last_update`, `last_update_fme`, `lat`, `long`) VALUES
(711156, 'SITRA2_RES_711156', 'RESTAURATION', 'Restaurant traditionnel', 'Le 21 sur Vin', '21 boulevard Eugène Deruelle', '69003', 'Lyon 3ème', '0478600656', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Lyon Tourisme et Congrès', 5167, '2015-10-13 07:36:03', '2015-10-13 08:05:34', '2015-10-13 22:08:27', 4.85446, 45.7632),
(747606, 'SITRA2_RES_747606', 'RESTAURATION', NULL, 'Brasserie les 3 brasseurs', '8 Chemin de Pontet et Crases', '69130', 'Ã‰cully', '0437460666', NULL, NULL, NULL, 'http:\\/\\/www.les3brasseurs.com', NULL, NULL, 'Ouvert tous les jours de 11h Ã  23h, sauf sam. jusqu''Ã  minuit et dim. jusqu''Ã  22:30.', NULL, NULL, NULL, 'Lyon Tourisme et CongrÃ¨s', 5213, '2015-12-23 08:15:44', '2015-12-23 09:03:42', '2015-12-23 23:09:53', 4.77452, 45.786);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `points`
--
ALTER TABLE `points`
 ADD UNIQUE KEY `ID` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
