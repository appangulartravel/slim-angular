<?php

/**
 * Configuration de la BD
 */
  $dbhost="127.0.0.1";
  $dbuser="root";
  $dbpass="0000";
  $dbname="lyon-point";
  
try {
    $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);  
} catch (PDOException $e) {
    echo 'Connexion échouée : ' . $e->getMessage();
}
  
  
  $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $dbh->exec("SET CHARACTER SET utf8");  

