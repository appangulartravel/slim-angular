<?php

require 'config/db.php';

require ('vendor/autoload.php');


$app = new \Slim\Slim(array(
    'debug' => true,
    'mode' => 'development'
));

//var_dump($dbh);
//phpinfo();


/**
 * Fonction GET /map
 * Renvoie un JSON contenant tous les Types de Points : Restauration, Hôtellerie, etc.
 * @return JSON
 */
$app->get('/map', function() use ($app, $dbh){
   
   //Récupérer les catégories
   $sql = 'SELECT DISTINCT type FROM points';
   $sth = $dbh->query($sql);
   
   $items = $sth->fetchAll(PDO::FETCH_OBJ);
   
   $json_types = json_encode($items);
   
   echo $json_types;
   
});


/**
 * Fonction GET /cron
 * Récupère le fichier des Datas du Grand-Lyon afin de remplir/mettre à jour la table
 * TODO : Faire une tâche CRON exécutant cette tâche
 */
$app->get('/cron', function() use ($app, $dbh) {
  try {
    echo 'cron debut '.date("Y-m-d H:i:s");  
    //Récupérer fichier json
    //5 premières données
    //$data_url = 'https://download.data.grandlyon.com/wfs/rdata?SERVICE=WFS&VERSION=2.0.0&outputformat=GEOJSON&maxfeatures=5&request=GetFeature&typename=sit_sitra.sittourisme&SRSNAME=urn:ogc:def:crs:EPSG::4171';
    //30 premieres données
    //$data_url = 'https://download.data.grandlyon.com/wfs/rdata?SERVICE=WFS&VERSION=2.0.0&outputformat=GEOJSON&maxfeatures=30&request=GetFeature&typename=sit_sitra.sittourisme&SRSNAME=urn:ogc:def:crs:EPSG::4171';
    //tous les points
    $data_url = 'https://download.data.grandlyon.com/wfs/rdata?SERVICE=WFS&VERSION=2.0.0&outputformat=GEOJSON&request=GetFeature&typename=sit_sitra.sittourisme&SRSNAME=urn:ogc:def:crs:EPSG::4171';
    //fichier local
    //$data_url = '/home/administrateur/www-dev/slim-angular/data_30.txt';
    $data_stream = fopen ( $data_url , 'r' );
    $data=stream_get_contents ( $data_stream );
  } catch(Exception $e){
    print_r('<br>erreur lors de l\'ouverture du fichier :'.$e.'<br>');
    exit;
  }
        
  $data_array = json_decode($data,true);
  $data_received = count($data_array['features']);
  $data_created = 0;
  $data_updated = 0;

  foreach ($data_array['features'] as $key => $value) {
    //var_dump($key,$value);
    $_id=$value['properties']['id'];
    //echo '<br>'.$_id.'<hr>';
    $_last_update =  $value['properties']['last_update'];
    $sql = 'SELECT `last_update` FROM `points` WHERE `ID`='.$_id;
    $sth = $dbh->query($sql);  
    
    $update = $sth->fetchall(PDO::FETCH_OBJ);      
      
    $id_sytra1=(isset($value['properties']['id_sitra1'])?$dbh->quote($value['properties']['id_sitra1']):'');
    $type=(isset($value['properties']['type'])?$dbh->quote($value['properties']['type']):'');
    $type_detail=(isset($value['properties']['type_detail'])?$dbh->quote($value['properties']['type_detail']):'');
    $nom=(isset($value['properties']['nom'])?$dbh->quote($value['properties']['nom']):'');
    $adresse=(isset($value['properties']['adresse'])?$dbh->quote($value['properties']['adresse']):'');
    $codepostal=(isset($value['properties']['codepostal'])?$dbh->quote($value['properties']['codepostal']):'');
    $commune=(isset($value['properties']['commune'])?$dbh->quote($value['properties']['commune']):'');
    $telephone=(isset($value['properties']['telephone'])?$dbh->quote($value['properties']['telephone']):'');
    $fax=(isset($value['properties']['fax'])?$dbh->quote($value['properties']['fax']):'');
    $telephonefax=(isset($value['properties']['telephonefax'])?$dbh->quote($value['properties']['telephonefax']):'');
    $email=(isset($value['properties']['email'])?$dbh->quote($value['properties']['email']):'');
    $siteweb=(isset($value['properties']['siteweb'])?$dbh->quote($value['properties']['siteweb']):'');
    $facebook=(isset($value['properties']['facebook'])?$dbh->quote($value['properties']['facebook']):'');
    $classement=(isset($value['properties']['classement'])?$dbh->quote($value['properties']['classement']):'');
    $ouverture=(isset($value['properties']['ouverture'])?$dbh->quote($value['properties']['ouverture']):'');
    $tarifsenclair=(isset($value['properties']['tarifsenclair'])?$dbh->quote($value['properties']['tarifsenclair']):'');
    $tarifsmin=(isset($value['properties']['tarifsmin'])?$dbh->quote($value['properties']['tarifsmin']):'');
    $tarifsmax=(isset($value['properties']['tarifsmax'])?$dbh->quote($value['properties']['tarifsmax']):'');
    $producteur=(isset($value['properties']['producteur'])?$dbh->quote($value['properties']['producteur']):'');
    $gid=(isset($value['properties']['gid'])?(int)($value['properties']['gid']):'');
    $date_creation=(isset($value['properties']['date_creation'])?$dbh->quote($value['properties']['date_creation']):'1970-00-00');
    $last_update=(isset($value['properties']['last_update'])?$dbh->quote($value['properties']['last_update']):'1970-00-00');
    $last_update_fme=(isset($value['properties']['last_update_fme'])?$dbh->quote($value['properties']['last_update_fme']):'1970-00-00');
    $lat = (float)$value['geometry']['coordinates'][1];
    $long = (float)$value['geometry']['coordinates'][0];
    $cu = count($update);
    if($cu==1){
     // var_dump($update[0]->last_update,$_last_update,'<hr>');

     //echo "la donnée est déjà en base".$_id;
     if($update[0]->last_update<$_last_update){
        //la donnée reçue est plus récente que la donnée stockée
        //var_dump('mise a jour');
        $sql = 'UPDATE `points` SET `id_sytra1`='.$id_sytra1.',`type`='.$type.',`type_detail`='.$type_detail.',`nom`='.$nom.',`adresse`='.$adresse.',`codepostal`='.$codepostal.',`commune`='.$commune.',`telephone`='.$telephone.',`fax`='.$fax.',`telephonefax`='.$telephonefax.',`email`='.$email.',`siteweb`='.$siteweb.',`facebook`='.$facebook.',`classement`='.$classement.',`ouverture`='.$ouverture.',`tarifsenclair`='.$tarifsenclair.',`tarifsmin`='.$tarifsmin.',`tarifsmax`='.$tarifsmax.',`producteur`='.$producteur.',`gid`='.$gid.',`date_creation`='.$date_creation.',`last_update`='.$last_update.',`last_update_fme`='.$last_update_fme.',`lattitude`='.$lat.',`longitude`='.$long.' WHERE `ID`='.$_id;
       // var_dump($sql);
        $items = $dbh->query($sql);
        //var_dump($items);
        if(isset($items))
          $data_updated++;

   
      }
    } elseif ($cu==0){
     // echo "la donnée reçue n'est pas encore en base".$_id;
      $sql = 'INSERT INTO `points`(`ID`, `id_sytra1`, `type`, `type_detail`, `nom`, `adresse`, `codepostal`, `commune`, `telephone`, `fax`, `telephonefax`, `email`, `siteweb`, `facebook`, `classement`, `ouverture`, `tarifsenclair`, `tarifsmin`, `tarifsmax`, `producteur`, `gid`, `date_creation`, `last_update`, `last_update_fme`, `lattitude`, `longitude`) 
              VALUES ("'.$_id.'","'.$id_sytra1.'","'.$type.'","'.$type_detail.'","'.$nom.'","'.$adresse.'","'.$codepostal.'","'.$commune.'","'.$telephone.'","'.$fax.'","'.$telephonefax.'","'.$email.'","'.$siteweb.'","'.$facebook.'","'.$classement.'","'.$ouverture.'","'.$tarifsenclair.'","'.$tarifsmin.'","'.$tarifsmax.'","'.$producteur.'","'.$gid.'","'.$date_creation.'","'.$last_update.'","'.$last_update_fme.'","'.$lat.'","'.$long.'")';
     // var_dump($sql);
      $sth = $dbh->query($sql);  
      if(isset($sth))
        $data_created++;
    } else {
        //erreur : identifiant non unique dans la table !
        print_r("<br>identifiant non unique dans la table ".$_id);
    }
  }
  echo ' fin cron '.date("Y-m-d H:i:s");
  echo ' nb données reçus '.$data_received;
  echo ' nb données créée '.$data_created;
  echo ' nb données modifiéées :'.$data_updated;
});



/**
 * Fonction GET /all
 * Retourne un JSON contenant l'ensemble des données contenues dans la table Points
 * @return JSON
 */
$app->get('/all', function() use ($app, $dbh) {

    //Récupérer point // Afficher point
    $sql = 'SELECT * FROM points';
    $sth = $dbh->query($sql);


    $items = $sth->fetchAll(PDO::FETCH_OBJ);
    $count = count($items);


    $array = array(
        //'nbItems' => $count,
        'type' => 'FeatureCollection',

    );


    foreach ($items as $k => $v) {
        $array['features'][$k]['type'] = 'Feature';
        
        $lat = (float)$v->lattitude;
        $long = (float)$v->longitude;
        unset($v->lattitude);
        unset($v->longitude);
        
        $array['features'][$k]['properties'] = $v;
        
 
        $array['features'][$k]['geometry'] = array(
            'type' => 'Point',
            'coordinates' => array($long, $lat)
        );
    }


    $json = json_encode($array);

    echo $json;
});



/**
 * Methode GET /points
 * Retourne tous les Points corresondant
 * @param String type   => Le type de point à récupérer 
 * @return JSON
 */
$app->get('/points/:type', function($type) use ($app, $dbh) {


    $sql = "SELECT * FROM points WHERE type = '$type'";
    
    $sth = $dbh->query($sql);
    
    $items = $sth->fetchAll(PDO::FETCH_OBJ);
    $count = count($items);
    
    
    //Formatage du JSON de retour
    $array = array(
        'nbItems' => $count,
        'type' => 'FeatureCollection',

    );


    foreach ($items as $k => $v) {
        $array['features'][$k]['type'] = 'Feature';
        
        $lat = (float)$v->lattitude;
        $long = (float)$v->longitude;
        unset($v->lat);
        unset($v->long);
        
        $array['features'][$k]['properties'] = $v;
        
 
        $array['features'][$k]['geometry'] = array(
            'type' => 'Point',
            'coordinates' => array($long, $lat)
        );
    }
     $json = json_encode($array);

    echo $json;            
});


/**
 * Méthode GET /around
 * Retourne les points d'un type donné autour d'une position donnée
 * @params String type / Float lattitude / float longitude / int distance = rayon
 * @return JSON
 */
$app->get('/around/:type/:lat/:long/:dist', function($type,$lat,$long,$dist) use ($app, $dbh) {
  //Latitude: 1 km = 1 deg / 110.54 km
//Longitude: 1 km = 1 deg / (111.320*cos(latitude) km)
    $deltaLat=abs((float)$dist/(2*110.54));
    $deltaLong=abs((float)$dist/(2*111.32*cos($lat)));
    $latMin=(float)$lat-$deltaLat;
    $latMax=(float)$lat+$deltaLat;
    $longMin=(float)$long-$deltaLong;
    $longMax=(float)$long+$deltaLong;
    $type_filtre = $dbh->quote($type);
    
    
    if(!strcmp($type_filtre,'\'all\'')||!strcmp($type_filtre, 'ALL')){
      $sql = "SELECT * FROM points 
              WHERE lattitude BETWEEN '$latMin' AND '$latMax'
              AND longitude BETWEEN '$longMin' AND '$longMax'";
    } else {
      $sql = "SELECT * FROM points 
              WHERE type = $type_filtre
              AND lattitude BETWEEN '$latMin' AND '$latMax'
              AND longitude BETWEEN '$longMin' AND '$longMax'";
    }

    $sth = $dbh->query($sql);
    
    $items = $sth->fetchAll(PDO::FETCH_OBJ);
    $count = count($items);
    
    $array = array(
        //'nbItems' => $count,
        'type' => 'FeatureCollection',

    );


    //Formattage du JSON
    foreach ($items as $k => $v) {
        $array['features'][$k]['type'] = 'Feature';
        
        $lat = (float)$v->lattitude;
        $long = (float)$v->longitude;
        unset($v->lattitude);
        unset($v->longitude);
        
        $array['features'][$k]['properties'] = $v;
        
 
        $array['features'][$k]['geometry'] = array(
            'type' => 'Point',
            'coordinates' => array($long, $lat)
        );
    }
     $json = json_encode($array);

    echo $json;            
});



$app->run();

