Bienvenue sur l'application App Travel

http://54.93.82.21/travel

L'application est encore en cours de développement, certaines features n'ont pas été développées par manque de temps.
Le design de l'application est assez sommaire mais pourra évoluer au fil du temps, des idées, et des collaborations.
La lecture de ce document vous permettra d'explorer les fonctionnalités de l'application.

========================================================================================================================
                                                    URL & fonctionnalités
========================================================================================================================

[A exécuter pour la première utilisation]
<host>/travel/api/index.php/cron    => Lance une fonction qui récupère le fichier JSON sur le site data.grandlyon.com afin de mettre à jour la BD
    Evolution : Faire une tâche cron éxecutant cette fonction

<host>/travel/#/home   => Affiche la map par défaut. Le JSON retourné est la liste des restaurants.
    Evolution à venir : Pouvoir mettre à jour la carte suivant le type d'établissement voulu, sa position initiale, le rayon voulu etc.

<host>/travel/#/all    => Affiche la liste des points récupérés via le fichier data du Grand-Lyon
    Evolution : Pouvoir trier sur les colonnes.

<host>/travel/#/add-point   => Affiche un formulaire permettant d'ajouter manuellement un point [non fonctionnel]
    Evolution : Ajouter une table SQL "points_perso" permettant d'afficher aussi les points ajoutés manuellement

<host>/travel/#/points/<type>   => Affiche la liste des points d'un type donné (ex : RESTAURATION, HOTELLERIE etc.)
    Evolution : Pouvoir afficher ces points sur la map

<host>/travel/#/around/<type>/<lat>/<long>/<rayon> => Affiche la liste des points d'une catégorie dans un carré de <rayon> km autour du point spếcifié, récupérés par un fichier json.
    Evolution : * ajouter une interface permettant à l'utilisateur d'y accéder facilement
                * afficher les résultats sur la carte